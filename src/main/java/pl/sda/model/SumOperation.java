package pl.sda.model;

import java.math.BigDecimal;

public class SumOperation implements Operation {
    private final BigDecimal firstArg;
    private final BigDecimal secondArg;

    //maybe that constructor will be common to all 2 args operations?
    public SumOperation(BigDecimal firstArg, BigDecimal secondArg) {
        this.firstArg = firstArg;
        this.secondArg = secondArg;
    }

    @Override
    public BigDecimal calculate() {
        return firstArg.add(secondArg);
    }
}
