package pl.sda.model;

import java.math.BigDecimal;

public interface Operation {
    BigDecimal calculate();
}
