package pl.sda;

import pl.sda.model.Operation;
import pl.sda.model.PairOfNumbers;
import pl.sda.model.SumOperation;

import java.math.BigDecimal;

public class EquationFactory {

    /**
     * That function produces 2 arg. Operations
     * @param pairOfNumbers pair of numbers to needed for operation
     * @param operatorSign for ex. *-/+,pow
     * @return Operation based on arguments provided
     */
    public Operation getTwoArgOperation(PairOfNumbers pairOfNumbers, String operatorSign) {
        //return new SumOperation(pairOfNumbers.getFirstArg(), pairOfNumbers.getSecondArg());
        throw new RuntimeException("Not implemented yet!");
    }

    /**
     * That function produces 2 arg. Operations
     * @param argument one number
     * @param operatorSign for ex. !,sqrt
     * @return Operation based on arguments provided
     */
    public Operation getOneArgOperation(BigDecimal argument, String operatorSign) {
        throw new RuntimeException("Not implemented yet!");
    }
}
